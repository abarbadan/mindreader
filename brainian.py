#F=dx^2/d^2t+kdx/dt
#F=dv/dt+kv
#(F-kv)dt+v=v
import numpy as np
import matplotlib.pyplot as plt
import random as rnd
from matplotlib import animation

class Box():

	def __init__(self, ngrid=256, npart=1000, friction=0.01, mass=5, mean_strength=10, in_headset=True, in_radial=True, in_rotational=False):


		self.n_particles=npart
		self.resolution=ngrid
		self.fric=friction
		self.m=mass
		self.F_mean=mean_strength

		self.v_x = np.zeros((self.n_particles))
		self.v_y = np.zeros((self.n_particles))

		self.x=np.random.normal(loc=0,scale=1.0,size=self.n_particles)
		self.y=np.random.normal(loc=0,scale=1.0,size=self.n_particles)

		self.image=np.zeros((self.resolution,self.resolution))

		self.x_int=np.rint(self.x/20*self.resolution+self.resolution/2).astype(np.int)
		self.y_int=np.rint(self.y/20*self.resolution+self.resolution/2).astype(np.int)

		self.image[self.x_int,self.y_int]=1

		self.prev_force_x = np.zeros(self.n_particles)
		self.prev_force_y = np.zeros(self.n_particles)

		self.headset = in_headset
		self.radial = in_radial
		self.rotational = in_rotational

	def update(self,F_eeg=0,multiplier=1,dt=0.05):
		self.image[self.x_int,self.y_int]=0

		if(self.headset): F_eeg*=multiplier
		else: F_eeg=0

		F_x=np.random.normal(loc=0,scale=self.F_mean,size=self.n_particles)
		F_y=np.random.normal(loc=0,scale=self.F_mean,size=self.n_particles)

		if(self.rotational):
			F_x+=-F_eeg*self.y/np.sqrt(self.x**2+self.y**2)
			F_y+=F_eeg*self.x/np.sqrt(self.x**2+self.y**2)
		if(self.radial):
			F_x+=F_eeg*self.x/np.sqrt(self.x**2+self.y**2)
			F_y+=F_eeg*self.y/np.sqrt(self.x**2+self.y**2)

		"""
		v_next_x = self.v_x + dt * self.m * 0.5 * (F_x + self.prev_force_x) - self.fric * (self.v_x + 0.5 * dt * self.m * self.prev_force_x)
		v_next_y = self.v_y + dt * self.m * 0.5 * (F_y + self.prev_force_y) - self.fric * (self.v_y + 0.5 * dt * self.m * self.prev_force_y)

		self.x += dt * 0.5 * (v_next_x + self.v_x)
		self.y += dt * 0.5 * (v_next_y + self.v_y)

		self.v_x = v_next_x
		self.v_y = v_next_y

		self.prev_force_x = F_x
		self.prev_force_y = F_y
		"""
		
		new_x=((F_x-self.fric*self.v_x)*dt*self.m+self.v_x)*dt+self.x
		new_y=((F_y-self.fric*self.v_y)*dt*self.m+self.v_y)*dt+self.y

		self.v_x=(F_x - self.fric*self.v_x)*dt*self.m
		self.v_y=(F_y - self.fric*self.v_y)*dt*self.m

		self.x=(new_x+10)%20-10
		self.y=(new_y+10)%20-10
		
		self.x_int=(np.rint(self.x/20*self.resolution+self.resolution/2).astype(np.int))%self.resolution
		self.y_int=(np.rint(self.y/20*self.resolution+self.resolution/2).astype(np.int))%self.resolution

		self.image[self.x_int,self.y_int]=1
		
	def spectrum(self):
		return np.fft.fftshift(np.abs(np.fft.fft2(self.image)))
