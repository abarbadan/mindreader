import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout
from brainian import Box
from tqdm import trange


def normalize_headset(headset, threshold = 1000):
    val = np.zeros(500)
    for i in trange(500):
        time.sleep(0.005)
        val[i] = headset.raw_value
        if np.abs(val[i]) > 1000:
            val[i] = 0
    avg = np.average(val)
    val = val - avg
    mx = np.max([np.max(val), -np.min(val)])
    
    return avg, mx


headset = mindwave.Headset('/dev/ttyUSB0')
time.sleep(2)


headset.connect()
print("Connecting...")

while headset.status != 'connected':
    time.sleep(0.5)
    print(headset.status)
    if headset.status == 'standby':
        headset.connect()
        print('Retrying connection...')
        
print("Connected")
time.sleep(2)

print("HOLD STILL, DON'T BLINK")
print(3)
time.sleep(1)
print(2)
time.sleep(1)
print(1)
time.sleep(1)
print('normalizing signal')
avg, mx = normalize_headset(headset)

t=Box(npart = 1000, friction = 1.0)

NN = t.resolution

fig = plt.figure()
ax = plt.axes(xlim=(-10,10), ylim=(-10,10))
x = np.linspace(-10,10,NN+1)[1:]
y = np.linspace(-10,10,NN+1)[1:]
y,x = np.meshgrid(y,x)
signal = np.zeros((NN,NN))
mp = ax.pcolormesh(x,y,signal,vmin=0,vmax=1)

def animation_init():
    global mp
    mp.set_array(t.image.ravel())
    return mp,
previous = 0
def animate(i):
    global headset
    global previous
    global avg
    global mx
    
    s = headset.raw_value
    if np.abs(s) > 1000:
        s = previous
    else:
        previous = s
    
    #t.update(F_eeg = -10*(s-avg)/mx, dt=0.15)
    t.update(0,dt = 0.15)
    mp.set_array(t.image[:-1,:-1].ravel())
    #mp.set_array(t.spectrum()[:-1,:-1].ravel())
    return mp,
    
# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=animation_init,
                               frames=1, interval=1, blit=True)
plt.show()
