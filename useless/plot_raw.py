import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout
from tqdm import trange


def normalize_headset(headset, threshold = 1000):
    val = np.zeros(500)
    for i in trange(500):
        time.sleep(0.005)
        val[i] = headset.raw_value
        if np.abs(val[i]) > 1000:
            val[i] = 0
    avg = np.average(val)
    val = val - avg
    mx = np.max([np.max(val), -np.min(val)])
    
    return avg, mx


headset = mindwave.Headset('/dev/ttyUSB0')
time.sleep(2)


headset.connect()
print("Connecting...")

while headset.status != 'connected':
    time.sleep(0.5)
    print(headset.status)
    if headset.status == 'standby':
        headset.connect()
        print('Retrying connection...')
        
print("Connected")
time.sleep(2)

print("HOLD STILL, DON'T BLINK")
print(3)
time.sleep(1)
print(2)
time.sleep(1)
print(1)
time.sleep(1)
print('normalizing signal')
avg, mx = normalize_headset(headset)


# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax1 = fig.add_subplot(1,2,1)
ax2 = fig.add_subplot(1,2,2)
ax1.set_xlim(0,1000)
ax1.set_ylim(-5,5)
ax2.set_xlim(0,1000)
ax2.set_ylim(-5,5)

t = np.arange(1000)
signal = np.zeros(1000)

line1, = ax1.plot(signal)
line2, = ax2.plot(signal)

# initialization function: plot the background of each frame
def init():
    global line1
    global line2
    global signal
    global t
    line1.set_data(t,signal)
    line2.set_data(t,signal)
    return line1, line2

# define globally the f space
# animation function.  This is called sequentially
previous = 0
def animate(i):
    global signal
    global t
    global avg
    global mx
    global previous
    signal = np.roll(signal,-1)
    s = headset.raw_value
    if np.abs(s) > 1000:
        s = previous
    else:
        previous = s
    signal[-1] = (s - avg)/mx
    
    line1.set_data(t,signal)
    line2.set_data(t,signal)
    #print(np.max(z))
    return line1, line2
    
# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()


    