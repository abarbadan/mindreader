import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout
time_reference = time.time()

headset = mindwave.Headset('/dev/ttyUSB0')
time.sleep(2)

headset.connect()
print("Connecting...")

while headset.status != 'connected':
    time.sleep(0.5)
    print(headset.status)
    if headset.status == 'standby':
        headset.connect()
        print('Retrying connection...')
        
print("Connected")
time_reference = time.time()
imp = headset.i
while True:
    time.sleep(10)
    print((headset.i - imp)/(time.time() - time_reference))
    



    