import numpy as np
import matplotlib.pyplot as plt
from tqdm import trange
class Image():

    def __init__(self, N_particles, m = 1.0, fric = 1.0, resolution = 1024, random_noise = True):
        self.N_particles = N_particles
        self.m = m
        self.fric = fric
        self.resolution = resolution
        self.random_noise = random_noise
        self.x = np.random.normal(loc = 0, scale = 0.1, size=N_particles)
        self.y = np.random.normal(loc = 0, scale = 0.1, size=N_particles)
        self.vx = np.zeros(N_particles)
        self.vy = np.zeros(N_particles)
        
        self.x_int = (self.resolution//2 + np.rint(self.x*(self.resolution//2)/10).astype(np.int))%self.resolution
        self.y_int = (self.resolution//2 + np.rint(self.y*(self.resolution//2)/10).astype(np.int))%self.resolution
        self.img = np.zeros((self.resolution, self.resolution))
    def pos_update(self, force, dt):
        #calc all radial forces here
        if self.random_noise:
            force = force + np.random.normal(loc = 0, scale = 1.0,size = (self.N_particles,2))
        self.vx = self.vx + dt*(force[:,0] - self.fric*self.vx)*self.m 
        self.vy = self.vy + dt*(force[:,1] - self.fric*self.vy)*self.m
        self.x = self.x + self.vx*dt
        self.y = self.y + self.vy*dt
        self.x = (self.x+10)%20 - 10
        self.y = (self.y+10)%20 - 10
        
    def generate_image(self):
        self.img[self.x_int, self.y_int] = 0
        self.x_int = (self.resolution//2 + np.rint(self.x*(self.resolution//2)/10).astype(np.int))%self.resolution
        self.y_int = (self.resolution//2 + np.rint(self.y*(self.resolution//2)/10).astype(np.int))%self.resolution
        self.img[self.x_int, self.y_int] = 1
        #return np.fft.fftshift(np.abs(np.fft.fft2(self.img)))
        return self.img

        