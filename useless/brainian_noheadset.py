import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout
from brainian import Box
from tqdm import trange

t=Box()

NN = t.resolution

fig = plt.figure()
ax = plt.axes(xlim=(-10,10), ylim=(-10,10))
x = np.linspace(-10,10,NN+1)[1:]
y = np.linspace(-10,10,NN+1)[1:]
y,x = np.meshgrid(y,x)
signal = np.zeros((NN,NN))
mp = ax.pcolormesh(x,y,signal,vmin=0,vmax=1)

def animation_init():
    global mp
    mp.set_array(t.image.ravel())
    return mp,

def animate(i):
    t.update()
    mp.set_array(t.image[:-1,:-1].ravel())
    #mp.set_array(t.spectrum()[:-1,:-1].ravel())
    return mp,
    
# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=animation_init,
                               frames=20, interval=1, blit=True)
plt.show()
