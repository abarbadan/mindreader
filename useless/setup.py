# $ python3 setup.py build_ext --inplace
# $ sudo python3
# >> import brainian_reader_gui

import os
from distutils.core import setup
from Cython.Build import cythonize

filelist =  ["brainian_reader_gui.py", "gui_win.py", "mindwave.py", "brainian.py"]

for filename in filelist:
	os.rename(filename, os.path.join(filename+'x'))
	setup(
	    ext_modules=cythonize(filename+'x'),
	)
	os.rename(filename+'x', os.path.join(filename))
