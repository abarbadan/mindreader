import glob
import os

filelist =  ["brainian_noheadset.pyx", "gui_win.pyx", "mindwave.pyx", "brainian.pyx"]

for i,filename in enumerate(glob.glob('*.pyx')):
    os.rename(filename, os.path.join(filename[:-1]))

