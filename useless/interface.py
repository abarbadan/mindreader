import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout

headset = mindwave.Headset('/dev/ttyUSB0')
time.sleep(2)


headset.connect()
print("Connecting...")

while headset.status != 'connected':
    time.sleep(0.5)
    print(headset.status)
    if headset.status == 'standby':
        headset.connect()
        print('Retrying connection...')
        
print("Connected")



N_f= 64
# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 1000), ylim=(0, N_f//2))
t = np.arange(1000)
f = np.arange(N_f//2)

f,t = np.meshgrid(f,t)
z = np.zeros((1000,N_f//2))
signal = np.zeros(N_f)

mp = ax.pcolormesh(t, f, z,vmin=0,vmax=13)

# initialization function: plot the background of each frame
def init():
    global mp
    global z
    mp.set_array(z.ravel())
    return mp,

# define globally the f space
# animation function.  This is called sequentially
def animate(i):
    global t
    global f
    global z
    global mp
    global signal
    
    signal = np.roll(signal,-N_f//2)
    for r in range(N_f//2):
        time.sleep(1.0/(8*N_f))
        signal[-N_f+r] = headset.raw_value
    
    z = np.roll(z,-1,axis = 0)
    spec = np.log(np.abs(np.fft.rfft(signal))[1:])
    
    z[-1,:] = spec
    mp.set_array(z[:-1,:-1].ravel())
    #print(np.max(z))
    return mp,
    
# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=1, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()


    