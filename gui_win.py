"""
Created on Fri Nov  2 17:24:18 2018

@author: TechnoPriest
"""

# import modules that I'm using
import matplotlib
matplotlib.use('TKAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import tkinter
from tkinter import *
import numpy as np
import scipy as sc
#import matplotlib.pyplot as pltlib
# lmfit is imported becuase parameters are allowed to depend on each other along with bounds, etc.



#Make object for application
class App_Window(tkinter.Tk):

    def __init__(self,N_param):
        tkinter.Tk.__init__(self)
        self.geometry("1000x800")
        self.title("Brainian Motion")
        self.initialize(N_param)

    def initialize(self,N_param):
        self.fig=plt.figure()
        #self.f = matplotlib.figure.Figure(figsize=(5,4),dpi=100)
        #FigSubPlot = Fig.add_subplot(111)
        self.pressed = False

        self.headset_pressed=False
        self.radial_pressed=False
        self.rotational_pressed=False

        self.headset_relief=True
        self.radial_relief=True
        self.rotational_relief=False

        x1=[]
        y1=[]

        x2=[]
        y2=[]

        x3=[]
        y3=[]

        x4=[]
        y4=[]

        self.button1 = tkinter.Button(self,text="Quit",command=self.client_exit,width=15)
        self.button1.pack(side=tkinter.BOTTOM)
        self.button2 = tkinter.Button(self,text="Acquire",command=self.start,width=15)
        self.button2.pack(side=tkinter.LEFT)
        self.button3 = tkinter.Button(self,text="Headset",command=self.headset,width=5,relief=SUNKEN)
        self.button3.pack(side=tkinter.TOP)
        self.button5 = tkinter.Button(self,text="Radial",command=self.radial,width=5,relief=SUNKEN)
        self.button5.pack(side=tkinter.TOP)
        self.button6 = tkinter.Button(self,text="Rotational",command=self.rotational,width=5,relief=RAISED)
        self.button6.pack(side=tkinter.TOP)


        self.var1 = tkinter.DoubleVar()
        self.var2 = tkinter.DoubleVar()
        self.var3 = tkinter.DoubleVar()
        self.var4 = tkinter.DoubleVar()
        self.force_mod = tkinter.DoubleVar()
        self.threshold = tkinter.DoubleVar()

        scale = tkinter.Scale(self, variable = self.var1, from_=10, to=0, resolution=0.1)
        scale.pack(side=tkinter.LEFT)

        scale1 = tkinter.Scale(self, variable = self.var2, from_=1000, to=0, resolution=1)
        scale1.pack(side=tkinter.LEFT)

        scale.place(relx=0.01, rely=0.65, anchor='sw')
        scale1.place(relx=0.065, rely=0.65, anchor='sw')

        ll = tkinter.Label(self, text='Mass')
        ll.place(relx=0.025, rely=0.68, anchor='sw')

        l2 = tkinter.Label(self, text='Particles')
        l2.place(relx=0.072, rely=0.68, anchor='sw')

        scale2 = tkinter.Scale(self, variable = self.var3, from_=10, to=0, resolution=0.1)
        scale2.pack(side=tkinter.LEFT)

        scale3 = tkinter.Scale(self, variable = self.var4, from_=10, to=0, resolution=0.1)
        scale3.pack(side=tkinter.LEFT)

        scale2.place(relx=0.01, rely=0.83, anchor='sw')
        scale3.place(relx=0.065, rely=0.83, anchor='sw')
        scale.set(1.5)
        scale1.set(500)
        scale2.set(2.9)
        scale3.set(8.8)

        scale4 = tkinter.Scale(self, variable = self.force_mod, from_=100, to=0, resolution=0.1)
        scale4.pack(side=tkinter.LEFT)
        scale4.place(relx=0.065, rely=0.40, anchor='sw')
        scale4.set(10)

        scale5 = tkinter.Scale(self, variable = self.threshold, from_=4, to=0, resolution=0.1)
        scale5.pack(side=tkinter.LEFT)
        scale5.place(relx=0.02, rely=0.40, anchor='sw')
        scale5.set(0)

        l3 = tkinter.Label(self, text='Friction')
        l3.place(relx=0.02, rely=0.86, anchor='sw')

        l4 = tkinter.Label(self, text='Strength')
        l4.place(relx=0.072, rely=0.86, anchor='sw')

        l5 = tkinter.Label(self, text='Mod')
        l5.place(relx=0.082, rely=0.43, anchor='sw')

        l6 = tkinter.Label(self, text='Thresh')
        l6.place(relx=0.02, rely=0.43, anchor='sw')

        self.axarr = [[self.fig.add_subplot(2,2,1),self.fig.add_subplot(2,2,2)],
                      [self.fig.add_subplot(2,2,3),self.fig.add_subplot(2,2,4)]]


        #self.line1, = axarr[0, 0].plot(x1, y1)
        #self.fig = plt.figure()
        #ax = plt.axes(xlim=(-10,10), ylim=(-10,10))
        self.x1 = np.linspace(-10,10,N_param+1)[1:]
        self.y1 = np.linspace(-10,10,N_param+1)[1:]
        self.x1,self.y1 = np.meshgrid(self.x1,self.y1)
        self.signal = np.zeros((N_param,N_param))
        self.line1 = self.axarr[0][0].pcolormesh(self.x1,self.y1,self.signal[:-1,:-1],vmin=0,vmax=1)
        self.line2 = self.axarr[0][1].pcolormesh(self.y1,self.x1,self.signal[:-1,:-1],vmin=0,vmax=100)


        self.line4, = self.axarr[1][1].plot([],[])
        #self.axarr[1][1].set_xlim(0,128)
        self.axarr[1][1].set_xlim(0,32)
        self.axarr[1][1].set_ylim(0,1)
        self.line3, = self.axarr[1][0].plot([],[])
        self.line5, = self.axarr[1][0].plot([],[],'r')
        self.axarr[1][0].set_xlim(0,64)
        self.axarr[1][0].set_ylim(-4,4)

        #self.line1, = FigSubPlot.plot(x,y,'r-')


        self.canvas = FigureCanvasTkAgg(self.fig, master=self)

        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
        self.canvas._tkcanvas.pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)
        self.resizable(True,False)
        self.update()


    def start(self):
        self.pressed = True
        self.line2 = self.axarr[0][1].pcolormesh(self.y1,self.x1,self.signal[:-1,:-1],vmin=0,vmax=self.var2.get()/5.0)
        self.line1 = self.axarr[0][0].pcolormesh(self.y1,self.x1,self.signal[:-1,:-1],vmin=0,vmax=1)

    def headset(self):
        self.headset_pressed=True
        if(self.headset_relief):
            self.button3.config(relief=RAISED)
            self.headset_relief=False
        else:
            self.button3.config(relief=SUNKEN)
            self.headset_relief=True

    def radial(self):
        self.radial_pressed=True
        if(self.radial_relief):
            self.button5.config(relief=RAISED)
            self.radial_relief=False
        else:
            self.button5.config(relief=SUNKEN)
            self.radial_relief=True

    def rotational(self):
        self.rotational_pressed=True
        if(self.rotational_relief):
            self.button6.config(relief=RAISED)
            self.rotational_relief=False
        else:
            self.button6.config(relief=SUNKEN)
            self.rotational_relief=True

    def client_exit(self):
        print("Goodbye, Jim.")
        exit()

if __name__ == "__main__":
    MainWindow = App_Window(None)

    x = np.array([1,2,3])
    y = np.array([1,2,3])
    MainWindow.Input_dat(x,y)
    MainWindow.mainloop()
