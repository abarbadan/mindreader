import matplotlib.pyplot as plt
import scipy.signal as ss
from matplotlib import animation
import numpy as np
import mindwave
import time
from matplotlib.pyplot import tight_layout
from brainian import Box
from tqdm import trange
import gui_win as gwin



def normalize_headset(headset, threshold = 1000):
    val = np.zeros(500)
    for i in trange(500):
        time.sleep(0.005)
        val[i] = headset.raw_value
        if np.abs(val[i]) > 1000:
            val[i] = 0
    avg = np.average(val)
    val = val - avg
    mx = np.max([np.max(val), -np.min(val)])
    collection = np.zeros(2010)
    hards = []
    softs = []

    return avg, mx #, np.average(np.array(hards)), np.average(np.array(softs))

headset = mindwave.Headset('/dev/ttyUSB0')
time.sleep(2)
headset.connect()
print("Connecting...")

while headset.status != 'connected':
    time.sleep(0.5)
    print(headset.status)
    if headset.status == 'standby':
        headset.connect()
        print('Retrying connection...')

print("Connected")
time.sleep(2)

print("HOLD STILL, DON'T BLINK")
print(3)
time.sleep(1)
print(2)
time.sleep(1)
print(1)
time.sleep(1)
print('normalizing signal')


# average, max, high, short
#avg, mx, h, s = normalize_headset(headset)
avg, mx = normalize_headset(headset)

# seriously?
#dec_threshold = (h+s)/2

# TODO define variables
NN = 128
bundle = 20
samp_freq = 1.0/450

#TODO figure out variable naming convention
MainWindow = gwin.App_Window(N_param = NN)

t=Box(ngrid = NN, npart = 500, friction = 1.0,mass=1.5,mean_strength=8.8)



button_click = False


sig = np.zeros(64)
big = np.zeros(64)
ti = np.linspace(0, 63,64)
fi = np.linspace(0,31,32)
def animation_init():
    global MainWindow
    MainWindow.line1.set_array([])
    MainWindow.line2.set_array([])
    MainWindow.line3.set_data([],[])
    MainWindow.line4.set_data([],[])
    MainWindow.line5.set_data([],[])
    return MainWindow.line1, MainWindow.line2, MainWindow.line3, MainWindow.line4, MainWindow.line5
previous = 0
time_reference = time.time()
def animate(i):
    global bundle
    global dec_threshold
    global samp_freq
    global time_reference
    global headset
    global previous
    global avg
    global mx
    global ti
    global fi
    global sig
    global big
    global MainWindow
    # modulation 8.8
    # mass 1.5
    # particles 500
    # friction 2.9
    # strength 8.8
    tot_force = 0
    s = headset.raw_value
    #s = np.random.normal()

    if np.abs(s) > 1000:
        s = previous
    else:
        previous = s
        
    tot_force = (s-avg)/mx
    if MainWindow.threshold.get()>1e-3 and MainWindow.threshold.get() < abs(tot_force):
        t.update(F_eeg = -MainWindow.force_mod.get()*abs(tot_force), dt=0.1)
    elif MainWindow.threshold.get()<=1e-3:
        t.update(F_eeg = -MainWindow.force_mod.get()*tot_force, dt=0.1)
    else:
        t.update(F_eeg = 0, dt = 0.1)


    #t.update(0,dt = 0.15)

    if MainWindow.pressed:
        t.__init__(ngrid = NN, npart=int(MainWindow.var2.get()), friction=MainWindow.var3.get(), mass=MainWindow.var1.get(), mean_strength=MainWindow.var4.get())
        MainWindow.pressed = False
    if MainWindow.headset_pressed:
        new_val_headset = not t.headset
        new_val_radial = t.radial
        new_val_rotational = t.rotational
        t.__init__(ngrid = NN, npart=int(MainWindow.var2.get()), friction=MainWindow.var3.get(), mass=MainWindow.var1.get(), mean_strength=MainWindow.var4.get(),in_headset=new_val_headset,in_radial=new_val_radial,in_rotational=new_val_rotational)
        MainWindow.headset_pressed = False

    if MainWindow.radial_pressed:
        new_val_headset = t.headset
        new_val_radial = not t.radial
        new_val_rotational = t.rotational
        t.__init__(ngrid = NN, npart=int(MainWindow.var2.get()), friction=MainWindow.var3.get(), mass=MainWindow.var1.get(), mean_strength=MainWindow.var4.get(),in_headset=new_val_headset,in_radial=new_val_radial,in_rotational=new_val_rotational)
        MainWindow.radial_pressed = False

    if MainWindow.rotational_pressed:
        new_val_headset = t.headset
        new_val_radial = t.radial
        new_val_rotational = not t.rotational
        t.__init__(ngrid = NN, npart=int(MainWindow.var2.get()), friction=MainWindow.var3.get(), mass=MainWindow.var1.get(), mean_strength=MainWindow.var4.get(),in_headset=new_val_headset,in_radial=new_val_radial,in_rotational=new_val_rotational)
        MainWindow.rotational_pressed = False




        #anim = animation.FuncAnimation(MainWindow.fig, animate, init_func=animation_init,
                                       #frames=1, interval=1, blit=True)


        #b=Box(ngrid = NN, npart = 1000, friction = 1.0)


    sig = np.roll(sig,-1)
    sig[-1] = (s-avg)/mx
    redline=np.array([MainWindow.threshold.get(),MainWindow.threshold.get()])
    MainWindow.line5.set_data(np.array([0,63]),redline)
    MainWindow.line1.set_array(t.image[:-1,:-1].ravel())
    MainWindow.line2.set_array(t.spectrum()[:-1,:-1].ravel())
    MainWindow.line3.set_data(ti, sig)
    f_spec = np.abs(np.fft.rfft(sig))[:-1]
    MainWindow.line4.set_data(fi, f_spec/np.max(f_spec))

    #MainWindow.canvas.draw()
    return MainWindow.line1,MainWindow.line2,MainWindow.line3,MainWindow.line4,MainWindow.line5

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(MainWindow.fig, animate, init_func=animation_init,
                               frames=1, interval=1, blit=True)



MainWindow.mainloop()
